
Steps to use this Starter project

1. Do a 'pod install' in this directory.

2. Open the MainApp workspace and the Info.plist file
    Change the following values

    1. Edit the key values: "KrumbsApplicationID" and "KrumbsApplicationClientKey" to use the application ID and client key of your Krumbs project.


3. Build and run the Krumbs Starter Project
