//
//  ProfileViewController.m
//  MainApp
//
//  Created by Asquith Bailey on 4/14/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "ProfileViewController.h"
extern NSString * LogoutNotification;
extern NSString * const SAVED_USER_ID_KEY;
extern NSString * const SAVED_USER_FIRSTNAME_KEY;
extern NSString * const SAVED_USER_LASTNAME_KEY;
extern NSString * const SAVED_USER_INVITE_CODE_KEY;
extern NSString * const SAVED_USER_IS_ANONYMOUS_KEY;

@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (readwrite, atomic) BOOL isAnonymousUser;

@end

@implementation ProfileViewController

-(instancetype) init {
    self = [super initWithNibName:@"ProfileViewController" bundle:[NSBundle bundleForClass:[self class]]];
    
    return self;
}
- (IBAction)settingsTapped:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Not yet implemented" message:@"Settings not yet implemented.." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)logoutTapped:(id)sender {
    if(self.isAnonymousUser) {
        [[NSNotificationCenter defaultCenter] postNotificationName:LogoutNotification object:nil userInfo:nil];
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Logging Out" message:@"Are you sure?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* acceptAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] postNotificationName:LogoutNotification object:nil userInfo:nil];
        }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:acceptAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.isAnonymousUser = [[NSUserDefaults standardUserDefaults] boolForKey:SAVED_USER_IS_ANONYMOUS_KEY];
    if(self.isAnonymousUser) {
        [self.logoutButton setTitle:@"Exit" forState:UIControlStateNormal];
        [[self.logoutButton titleLabel] setFont:[UIFont fontWithName:@"AvenirNextCondensed-Regular" size:14.0]];
        [self.welcomeLabel setText:[NSString stringWithFormat:@"Welcome, Guest!"]];
    } else {
        
        NSString* userID = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USER_ID_KEY];
        BOOL isInvitee = [[NSUserDefaults standardUserDefaults] boolForKey:SAVED_USER_INVITE_CODE_KEY];
        NSString *fn = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USER_FIRSTNAME_KEY];
        NSString *sn = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USER_LASTNAME_KEY];
        
        if(fn != nil && sn != nil) {
            [self.welcomeLabel setText:[NSString stringWithFormat:@"Welcome, %@ %@",fn, sn ]];
        }
        if(isInvitee == YES) {
            [self.welcomeLabel setText:[NSString stringWithFormat:@"Welcome, Invitee #%@",userID]];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
