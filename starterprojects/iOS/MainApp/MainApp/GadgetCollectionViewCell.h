//
//  GadgetCollectionViewCell.h
//  KDCBidApp
//
//  Created by Asquith Bailey on 6/11/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GadgetCollectionViewCell : UICollectionViewCell

+ (void)registerWithCollectionView:(UICollectionView*)collectionView;
+(NSString *) reusableIdentifier;

@end
