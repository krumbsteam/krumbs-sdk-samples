//
//  SignInViewController.m
//  MainApp
//
//  Created by Asquith Bailey on 4/13/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "SignInViewController.h"
#import "SHEmailValidator.h"
extern NSString *LoginCompleteNotification;

@interface SignInViewController ()
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;

@end

@implementation SignInViewController
-(void) checkEnabled {
    if( [self.emailAddressTextField text] != nil) {
        NSError *error = nil;
        BOOL isValidEmail = [[SHEmailValidator validator] validateSyntaxOfEmailAddress:[self.emailAddressTextField text] withError:&error];
        if(error == nil && isValidEmail) {
            [self.signInButton setEnabled:YES];
        }  else {
        [self.signInButton setEnabled:NO];
        }
    } else {
        [self.signInButton setEnabled:NO];        
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.signInButton setTitleColor:[UIColor colorWithRed:0.600 green:0.600 blue:0.600 alpha:1.00] forState:UIControlStateDisabled];
    [self.signInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
    [self checkEnabled];
}
- (IBAction)emailEditingCompleted:(id)sender {
    [self checkEnabled];
}


-(void) openLandingPage:(BOOL) isAnonymous {
    NSDictionary *userDic = [NSMutableDictionary new];
    if(!isAnonymous) {
        [userDic setValue:[self.emailAddressTextField text] forKey:@"Email"];
        [userDic setValue:[self.firstNameTextField text] forKey:@"FirstName"];
        [userDic setValue:[self.lastNameTextField text] forKey:@"LastName"];
    } else {
        [userDic setValue:[NSNumber numberWithBool:TRUE] forKey:@"AnonymousLogin"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LoginCompleteNotification object:nil userInfo:userDic];
}

- (IBAction)signInWithEmail:(id)sender {
    [self openLandingPage: FALSE];
}

- (IBAction)anonymousSignIn:(id)sender {
    [self openLandingPage: TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
