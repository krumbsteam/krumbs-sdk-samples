//
//  LandingPageViewController.h
//  MainApp
//
//  Created by Asquith Bailey on 4/12/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "ProfileViewController.h"

@interface LandingPageViewController : UITabBarController <UITabBarControllerDelegate>

@property (nonatomic,retain) HomeViewController *homeVC;
@property (nonatomic,retain) ProfileViewController *profileVC;
@property (nonatomic, retain) NSMutableDictionary* attributes;
@property (nonatomic, retain) NSMutableDictionary* selectedAttributes;

-(void) setSDKReady:(BOOL) readyState;
@end

