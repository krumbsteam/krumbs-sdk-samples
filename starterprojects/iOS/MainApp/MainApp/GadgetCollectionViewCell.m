//
//  GadgetCollectionViewCell.m
//  KDCBidApp
//
//  Created by Asquith Bailey on 6/11/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "GadgetCollectionViewCell.h"

@implementation GadgetCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

//    GadgetCollectionViewCell *gcell = (GadgetCollectionViewCell *)cell;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.cornerRadius = 6.0;
    self.backgroundColor = [UIColor whiteColor];
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    self.layer.shadowRadius = 2.0f;
    self.layer.shadowOpacity = 0.6;
    self.layer.masksToBounds = NO;
}

+ (void)registerWithCollectionView:(UICollectionView*)collectionView {
    NSString* className = NSStringFromClass([self class]);
    NSBundle* bundle = [NSBundle bundleForClass:[self class]];
    [collectionView registerNib:[UINib nibWithNibName:className bundle:bundle] forCellWithReuseIdentifier:[GadgetCollectionViewCell reusableIdentifier]];
}

+(NSString *) reusableIdentifier {
    return  NSStringFromClass([self class]);
}

@end
