//
//  HomeViewController.m
//  MainApp
//
//  Created by Asquith Bailey on 4/14/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "HomeViewController.h"
#import "GadgetCollectionViewCell.h"
#import "ActionSheetStringPicker.h"
#import <FontAwesomeKit/FontAwesomeKit.h>
#import <KrumbsSDK/KrumbsSDK.h>

extern NSString * LogoutNotification;
extern NSString * const SAVED_APP_DOMAIN_KEY;
extern NSString * const SAVED_USER_IS_ANONYMOUS_KEY;

NSString *const KCaptureCompleteNotification = @"io.krumbs.sdk.app.CaptureCompleted";
NSString *const KCaptureDiscardedNotification = @"io.krumbs.sdk.app.CaptureDiscarded";
NSString *const CaptureStartedNotification = @"io.krumbs.sdk.app.CaptureStarted";

NSString *const TimePeriodNotificationSubject = @"io.krumbs.sdk.ios.GadgetTimePeriodNotification";


@interface HomeViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *gadgetCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (strong, nonatomic) NSArray<NSNumber *> *dashboardGadgets;
@property (weak, nonatomic) IBOutlet UILabel *currentTimePeriodLabel;

@property (strong, nonatomic) NSMutableDictionary *gadgetNavKeys;

@property (readwrite, atomic) BOOL isAnonymousUser;

@property (readwrite, atomic) NSInteger currentSelectedTimePeriod;

@end

@implementation HomeViewController


- (IBAction)filterTapped:(id)sender {
    
    
    NSArray *timeperiods = [NSArray arrayWithObjects:@"Today", @"Past 24 Hours", @"Past Month", @"Past Year", nil];
    
    NSArray *timeperiodNotifObjects = [NSArray arrayWithObjects:[NSNumber numberWithInteger:Today],
                                       [NSNumber numberWithInteger:Last24Hours],
                                       [NSNumber numberWithInteger:Last30Days],
                                       [NSNumber numberWithInteger:Last12Months], nil];
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"Filter" rows:timeperiods
                                                                    initialSelection:self.currentSelectedTimePeriod
                                                                           doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                                                              // NSLog(@"Picker: %@", picker);
                                                                               [[NSNotificationCenter defaultCenter] postNotificationName:TimePeriodNotificationSubject object:[timeperiodNotifObjects objectAtIndex:selectedIndex]];
                                                                               self.currentTimePeriodLabel.text = (NSString *)selectedValue;
                                                                               self.currentSelectedTimePeriod = selectedIndex;
                                                                               
                                                                           }
                                                                         cancelBlock:^(ActionSheetStringPicker *picker) {
                                                                             NSLog(@"Block Picker Canceled");
                                                                             
                                                                         }
                                                                              origin:sender];
    
    picker.tapDismissAction = TapActionSuccess;
    [picker showActionSheetPicker];
    
}

-(instancetype) init {
    self = [super initWithNibName:@"HomeViewController" bundle:[NSBundle bundleForClass:[self class]]];
    self.currentSelectedTimePeriod = 0;
    return self;
}



-(void) setupGadgets
{
    UIViewController *whatVc = (UIViewController *)[[KrumbsSDK sharedInstance] gadgetUIViewControllerForView:What_Reports];
    UIViewController *whoVc = (UIViewController *)[[KrumbsSDK sharedInstance] gadgetUIViewControllerForView:Who_People];
    
    UIViewController *whyVc = (UIViewController *)[[KrumbsSDK sharedInstance] gadgetUIViewControllerForView:Why_TopIntents];
    
    UIViewController *whereVc = (UIViewController *)[[KrumbsSDK sharedInstance] gadgetUIViewControllerForView:Where_TopPlaces];
    
    self.gadgetNavKeys = [[NSMutableDictionary alloc] initWithCapacity:4];
    self.dashboardGadgets = [NSArray arrayWithObjects:
                             [NSNumber numberWithInt:What_Reports],
                             [NSNumber numberWithInt:Who_People ],
                             [NSNumber numberWithInt:Why_TopIntents],
                             [NSNumber numberWithInt:Where_TopPlaces],
                             nil];
    
    [self.gadgetNavKeys setObject:whatVc forKey:[NSNumber numberWithInt:What_Reports]];
    [self.gadgetNavKeys setObject:whoVc forKey:[NSNumber numberWithInt:Who_People]];
    [self.gadgetNavKeys setObject:whyVc forKey:[NSNumber numberWithInt:Why_TopIntents]];
    [self.gadgetNavKeys setObject:whereVc forKey:[NSNumber numberWithInt:Where_TopPlaces]];
    
}

-(void) setupViews {
    
    [self setupGadgets];
    
    // configure filter button UI
    CGFloat iconSize = 30.0f;
    CGSize imageSize = CGSizeMake(iconSize, iconSize);
    
    FAKFontAwesome *filterIcon = [FAKFontAwesome slidersIconWithSize:iconSize];
    [filterIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *fgFilterImage = [[filterIcon imageWithSize:imageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //    [filterIcon addAttribute:NSForegroundColorAttributeName value:highlightColor];
    //    UIImage *selHomeImage = [[homeIcon imageWithSize:imageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //    UIImage* tabItemImage;
    
    [self.filterButton setImage:fgFilterImage forState:UIControlStateNormal];
    
    //    self.filterButton.layer.borderColor = [UIColor whiteColor].CGColor;
    //    self.filterButton.layer.borderWidth = 1.0;
    
    
    // register for SDK callbacks
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureCompletedAction:)
                                                 name:KCaptureCompleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureCompletedAction:)
                                                 name:KCaptureDiscardedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureStartedAction:)
                                                 name:CaptureStartedNotification object:nil];
    
    // setup dashboard with SDK gadgets
    [GadgetCollectionViewCell registerWithCollectionView:self.gadgetCollectionView];
    
      [self.gadgetCollectionView reloadData];
    
}


#pragma mark UICollectionViewDelegate methods
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GadgetCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:[GadgetCollectionViewCell reusableIdentifier] forIndexPath:indexPath];
    return cell;}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIViewController *cellVC = [self.gadgetNavKeys objectForKey:[NSNumber numberWithInteger:indexPath.item]];
   
    [cell addSubview:cellVC.view];
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *cellVC = [self.gadgetNavKeys objectForKey:[NSNumber numberWithInteger:indexPath.item]];
    
    float margin = 10.0f;

    float screenWidth = [UIScreen mainScreen].bounds.size.width;
//    CGSize gadgetSize = CGSizeMake(screenWidth-2*margin, cellVC.view.frame.size.height);
    CGSize gadgetSize = CGSizeMake(screenWidth-2*margin, cellVC.view.frame.size.height);
//    cellVC.view.frame = CGRectMake(cellVC.view.frame.origin.x, cellVC.view.frame.origin.y, gadgetSize.width, gadgetSize.height);
    cellVC.view.frame = CGRectMake(0, 0, gadgetSize.width, gadgetSize.height);
    cellVC.view.layer.masksToBounds = YES;
    
    return gadgetSize;
//    return cellVC.view.frame.size;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dashboardGadgets count];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(void) viewWillDisappear:(BOOL)animated
{
    
    
}
-(void) captureCompletedAction:(NSNotification *)notification
{
    
}

-(void) captureStartedAction:(NSNotification *)notification
{
    
}


- (IBAction)logoutTapped:(id)sender {
    
    if(self.isAnonymousUser) {
        [[NSNotificationCenter defaultCenter] postNotificationName:LogoutNotification object:nil userInfo:nil];
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Logging Out" message:@"Are you sure?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* acceptAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] postNotificationName:LogoutNotification object:nil userInfo:nil];
        }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:acceptAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.isAnonymousUser = [[NSUserDefaults standardUserDefaults] boolForKey:SAVED_USER_IS_ANONYMOUS_KEY];
    if(self.isAnonymousUser) {
        [self.logoutButton setTitle:@"Exit" forState:UIControlStateNormal];
        [[self.logoutButton titleLabel] setFont:[UIFont fontWithName:@"AvenirNextCondensed-Regular" size:14.0]];
    }
    
    [self setupViews];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
