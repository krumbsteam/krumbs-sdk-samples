//
//  LandingPageViewController.m
//  MainApp
//
//  Created by Asquith Bailey on 4/12/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "LandingPageViewController.h"
#import <FontAwesomeKit/FontAwesomeKit.h>
#import <KrumbsSDK/KrumbsSDK.h>
extern NSString *const KCaptureControllerImageUrl;
extern NSString *const KCaptureControllerAudioUrl;
extern NSString *const KCaptureControllerMediaJsonUrl;
extern NSString *const KCaptureControllerIsAudioCaptured;
extern NSString *const KCaptureCompleteNotification;
extern NSString *const KCaptureDiscardedNotification;

extern NSString *const CaptureStartedNotification;
typedef enum {
    kFSApplicationStateMe = 2,
    kFSApplicationStateHome = 0
} FSApplicationState;

@interface LandingPageViewController () <KCaptureViewControllerDelegate>
//@property (strong, nonatomic) IBOutlet WKWebView *webView;

@property (atomic) BOOL reportReady;
@end

@implementation LandingPageViewController
@synthesize  profileVC, homeVC, attributes, selectedAttributes;

//static LandingPageViewController* sharedLandingPage;

//+(id) singleton {
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedLandingPage = [LandingPageViewController new];
//    });
//    
//    return sharedLandingPage;
//}

- (id) init {
    self=[super init];
    if(self)
    {
        self.reportReady = NO;
 
        [self setupView];
    }
    return self;
}

-(void) setSDKReady:(BOOL) readyState
{
    self.reportReady = readyState;
    [self enabledStartButton:self.reportReady];

}

-(void)setupView
{
    self.delegate = self;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    NSMutableDictionary* vcMap = [[NSMutableDictionary alloc] init] ;
    self.tabBar.translucent = YES;
    CGFloat iconSize = 30.0f;
    CGSize imageSize = CGSizeMake(iconSize, iconSize);
    //Home
    self.homeVC = [[HomeViewController alloc] init] ;
    UINavigationController* homeNav = [[UINavigationController alloc] initWithRootViewController:self.homeVC] ;
    UIColor *foregroundColor = [UIColor colorWithRed:0.475 green:0.475 blue:0.475 alpha:1.00];
    UIColor *highlightColor = [UIColor colorWithRed:0.086 green:0.494 blue:0.984 alpha:1.00];
    FAKFontAwesome *homeIcon = [FAKFontAwesome homeIconWithSize:iconSize];
    [homeIcon addAttribute:NSForegroundColorAttributeName value:foregroundColor];
    UIImage *fgHomeImage = [[homeIcon imageWithSize:imageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [homeIcon addAttribute:NSForegroundColorAttributeName value:highlightColor];
    UIImage *selHomeImage = [[homeIcon imageWithSize:imageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIImage* tabItemImage;
    
    self.homeVC.tabBarItem.image = fgHomeImage;
    self.homeVC.tabBarItem.selectedImage = selHomeImage;
    
    homeNav.tabBarItem.title = @"Home";
    self.homeVC.navigationItem.title = @"Home";
    homeNav.navigationBar.translucent = YES;
    [homeNav setNavigationBarHidden:YES];
    //    [homeNav.navigationBar setBarTintColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.9]];
    
    [vcMap setObject:homeNav forKey:[NSNumber numberWithInt:kFSApplicationStateHome]];
    
    //Profile
    
    self.profileVC = [[ProfileViewController alloc] init] ;
    //    self.profileVC.userModel = [CSUserModel currentUser];
    UINavigationController *profileNav = [[UINavigationController alloc] initWithRootViewController:self.profileVC] ;
    profileNav.tabBarItem.title = @"Profile";
    
    FAKFontAwesome *mineIcon = [FAKFontAwesome userIconWithSize:iconSize];
    [mineIcon addAttribute:NSForegroundColorAttributeName value:foregroundColor];
    UIImage *fgMineImage = [[mineIcon imageWithSize:imageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [mineIcon addAttribute:NSForegroundColorAttributeName value:highlightColor];
    UIImage *selMineImage = [[mineIcon imageWithSize:imageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    self.profileVC.navigationItem.title = @"Profile";
    profileNav.tabBarItem.image = fgMineImage;
    profileNav.tabBarItem.selectedImage = selMineImage;
    profileNav.navigationBar.translucent = YES;
    [profileNav.navigationBar setBarTintColor:[UIColor whiteColor]];
    [profileNav setNavigationBarHidden:YES];
    
    [vcMap setObject:profileNav forKey:[NSNumber numberWithInt:kFSApplicationStateMe]];
    
    
    NSMutableArray* tabBarVCs = [[NSMutableArray alloc] init];
    for(int i = 0; i < 3; i++)
    {
        NSNumber* index = [NSNumber numberWithInt:i];
        if([vcMap objectForKey:index] != nil) {
            [tabBarVCs addObject:[vcMap objectForKey:index]];
        }
    }
    
    UIViewController* dummyCenterViewController = [[UIViewController alloc] init];
    dummyCenterViewController.tabBarItem.title = @"Report!";
    [tabBarVCs insertObject:dummyCenterViewController atIndex:1];
//    tabItemImage = [[UIImage imageNamed:@"StartButtonImage"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

//    dummyCenterViewController.tabBarItem.image = tabItemImage;
    
    
//    UIImage *tabItemHighlightedImage = [[UIImage imageNamed:@"StartButtonImageHighlighted"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    dummyCenterViewController.tabBarItem.selectedImage = tabItemHighlightedImage;
    

    self.viewControllers = tabBarVCs;
    
    self.tabBar.tintColor=highlightColor;
    self.tabBar.barTintColor = [UIColor colorWithRed:0.957 green:0.957 blue:0.957 alpha:1.00];
    
    self.tabBar.translucent=YES;
    
    
    self.attributes = [[NSMutableDictionary alloc] init];
    
    [attributes setObject:[UIColor colorWithRed:0.600 green:0.600 blue:0.600 alpha:1.00] forKey:NSForegroundColorAttributeName];
    [attributes setObject:[UIFont fontWithName:@"AvenirNextCondensed-Regular" size:10.0] forKey:NSFontAttributeName];
    
    
    self.selectedAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
    [selectedAttributes setObject:highlightColor forKey:NSForegroundColorAttributeName];
    [profileNav.tabBarItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [profileNav.tabBarItem setTitleTextAttributes:selectedAttributes forState:UIControlStateSelected];
    [homeNav.tabBarItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [homeNav.tabBarItem setTitleTextAttributes:selectedAttributes forState:UIControlStateSelected];
    
    
    [dummyCenterViewController.tabBarItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [dummyCenterViewController.tabBarItem setTitleTextAttributes:selectedAttributes forState:UIControlStateSelected];

    
}



#pragma mark - Tab Bar Controller Delegate
-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSInteger index = [self.viewControllers indexOfObject:viewController];
    return (index != 1);
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    //    [(LandingPageViewController*)tabBarController navigationItemSelected];
}

-(void) viewWillAppear:(BOOL)animated {
    UIImage *tabItemImage = [[UIImage imageNamed:@"StartButtonImage"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *tabItemHighlightedImage = [[UIImage imageNamed:@"StartButtonImageHighlighted"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self addCenterButtonWithImage:tabItemImage highlightImage:tabItemHighlightedImage];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITabBarDelegate
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
/*    if(item == [self.viewControllers[1] tabBarItem]) {
        [self reportButtonTapped];
    }
  */  
}

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    
    UIButton *centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    centerButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [centerButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [centerButton setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    [centerButton addTarget:self action:@selector(reportButtonTapped) forControlEvents:UIControlEventTouchUpInside];

    
    centerButton.center = CGPointMake(self.tabBar.frame.size.width/2,
                                           self.tabBar.frame.size.height/2);
    
//    self.centerButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        centerButton.center = CGPointMake(self.tabBar.center.x, self.tabBar.frame.size.height/2);
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        centerButton.center = center;
    }
    CGPoint finalCenter = centerButton.center;
    finalCenter.y -= 14.0f; // offset text height
    centerButton.center = finalCenter;
    
    [self.view addSubview:centerButton];
    [self.view bringSubviewToFront:centerButton];
    
    //Keep this Button Hide till we get ready handler from Krumbs SDK
    centerButton.tag = 1001;
    [self enabledStartButton:self.reportReady];
}

-(void)enabledStartButton:(BOOL)state
{
    UIButton *centerButton = [self.view viewWithTag:1001];
    centerButton.enabled = state;
    UITabBarItem *item =   self.tabBar.items [1];
    item.title = state? @"Report!" : @"Loading...";
}



-(void) reportButtonTapped {
    // start the KCapture component in your view controller and register a delegate for callback when complete
    KCaptureViewController* vc = [[KrumbsSDK sharedInstance] startKCaptureViewController];
    vc.delegate = self;

    [self.selectedViewController dismissViewControllerAnimated:NO completion:nil];
    
    [self.selectedViewController presentViewController:vc animated:NO completion:^{
        NSLog(@"capture view controller presented..");
        
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CaptureStartedNotification object:@"LandingPage" userInfo:nil];

//    [self presentViewController:vc animated:YES completion:nil];
}

- (void) captureController:(KCaptureViewController *)captureController
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)media {
    //    self.imageView.image = [media objectForKey:KCaptureControllerImageUrl];
    NSURL *mediaJsonUrl = [media objectForKey:KCaptureControllerMediaJsonUrl];
    NSLog(@"MediaJSONUrl: %@",mediaJsonUrl);
    BOOL audioWasCaptured = [(NSString *)[media objectForKey:KCaptureControllerIsAudioCaptured] boolValue];
    NSLog(@"Audio was captured: %d",audioWasCaptured);
    if(audioWasCaptured) {
        NSURL *localAudioUrl = [media objectForKey:KCaptureControllerAudioUrl];
        NSLog(@"Local Audio Url: %@",localAudioUrl);
    }
    kCaptureCompletionState completionState = [(NSString *)[media objectForKey:KCaptureControllerCompletionState] intValue];
    NSLog(@"Capture Completion state: %d",completionState);
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KCaptureCompleteNotification object:@"KCaptureControllerDelegate" userInfo:media];
}

- (void)captureControllerDidCancel:(KCaptureViewController *)captureController {
    NSLog(@"User cancelled capture!");

    [[NSNotificationCenter defaultCenter] postNotificationName:KCaptureDiscardedNotification object:@"KCaptureControllerDelegate" userInfo:nil];

}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
