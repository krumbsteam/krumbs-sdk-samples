//
//  LoginWithCodeViewController.m
//  MainApp
//
//  Created by Asquith Bailey on 4/13/16.
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "LoginWithCodeViewController.h"

extern NSString *LoginCompleteNotification;

@interface LoginWithCodeViewController ()
@property (weak, nonatomic) IBOutlet UITextField *loginCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginWithCodeButton;

@end

@implementation LoginWithCodeViewController

-(void) checkEnabled {
    if( [self.loginCodeTextField text] != nil && [[self.loginCodeTextField text] isEqualToString:@""]) {
        [self.loginWithCodeButton setEnabled:NO];
    }
    else {
        [self.loginWithCodeButton setEnabled:YES];
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.loginWithCodeButton setTitleColor:[UIColor colorWithRed:0.600 green:0.600 blue:0.600 alpha:1.00] forState:UIControlStateDisabled];
    [self.loginWithCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
    [self checkEnabled];
}

-(void) openLandingPage:(BOOL) isAnonymous {
    NSDictionary *userDic = [NSMutableDictionary new];
    if(!isAnonymous) {
        [userDic setValue:[self.loginCodeTextField text] forKey:@"InviteCode"];
    }
    else {
        [userDic setValue:[NSNumber numberWithBool:TRUE] forKey:@"AnonymousLogin"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:LoginCompleteNotification object:nil userInfo:userDic];
}
- (IBAction)editingEnded:(id)sender {
    [self checkEnabled];
}

- (IBAction)loginWithCode:(id)sender {
    [self openLandingPage:FALSE];
}

- (IBAction)anonymousLogin:(id)sender {
    
    [self openLandingPage:TRUE];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
