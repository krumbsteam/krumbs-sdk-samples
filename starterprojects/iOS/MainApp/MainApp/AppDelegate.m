//
//  AppDelegate.m
//  KrumbsSDK
//
//  Copyright © 2016 Krumbs Inc. All rights reserved.
//

#import "AppDelegate.h"
#import <KrumbsSDK/KrumbsSDK.h>
#import <FontAwesomeKit/FontAwesomeKit.h>
#import "LandingPageViewController.h"


NSString * const LoginCompleteNotification = @"io.krumbs.apptemplate.LoginCompleted";
NSString * const LogoutNotification = @"io.krumbs.apptemplate.LogoutEvent";
NSString * const SAVED_USER_ID_KEY = @"Current User ID";
NSString * const SAVED_USER_FIRSTNAME_KEY = @"Current User First Name";
NSString * const SAVED_USER_LASTNAME_KEY = @"Current User Last Name";
NSString * const SAVED_USER_INVITE_CODE_KEY = @"Current User InviteCode Login";
NSString * const SAVED_APP_DOMAIN_KEY = @"Current Krumbs Application Domain";
NSString * const SAVED_USER_IS_ANONYMOUS_KEY = @"Current User Is Anonymous";

NSString * const KRUMBS_APPLICATION_ID_KEY = @"KrumbsApplicationID";
NSString * const KRUMBS_APPLICATION_CLIENTKEY_KEY = @"KrumbsApplicationClientKey";


@interface AppDelegate ()
@property (nonatomic, strong) NSString *myAppDomain;
@property (atomic) BOOL sdkReady;
@end

@implementation AppDelegate






- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSString *appID = [[NSBundle mainBundle] objectForInfoDictionaryKey:KRUMBS_APPLICATION_ID_KEY];
    NSString *clientKey =[[NSBundle mainBundle] objectForInfoDictionaryKey:KRUMBS_APPLICATION_CLIENTKEY_KEY];
    
    
    // Initialize the API
    
    [KrumbsSDK initWithApplicationID:appID andClientKey:clientKey];

    
    // Configure the color scheme of the IntentPanel
    KIntentPanelConfiguration *uiConfig = [[KrumbsSDK sharedInstance] getIntentPanelViewConfigurationDefaults];
    [uiConfig setIntentPanelBarColor:[UIColor colorWithRed:0.075 green:0.522 blue:0.659 alpha:1.00]];
    [uiConfig setIntentPanelBarTextColor:[UIColor whiteColor]];
    
    [[KrumbsSDK sharedInstance] setIntentPanelViewConfigurationDefaults:uiConfig];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sdkReadyToUseHandler:) name:KrumbsSDKReadyToUseNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sdkInitializationFailureHandler:) name:KrumbsSDKInitializationFailureNotification object:nil];

    self.window=[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
   
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginCompletedAction:)
                                                 name:LoginCompleteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutAction:)
                                                 name:LogoutNotification object:nil];
    
    
    NSBundle* frameworkBundle = [NSBundle bundleForClass:[self class]];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [FAKFontAwesome registerIconFontWithURL:[frameworkBundle URLForResource:@"FontAwesome" withExtension:@"otf"]];
    });
    
    
    //Attempt to automatically log the user in
    if(![self attemptToSkipLogin]) {
        [self showLoginSignup];
    }
    
    
    return YES;
}

-(void)sdkReadyToUseHandler:(NSNotification*)notification
{
    NSLog(@"SDK ready");
    self.sdkReady = YES;


}

-(void)sdkInitializationFailureHandler:(NSNotification*)notification
{
    NSLog(@"SDK failed to initialize");
    self.sdkReady = NO;
}

-(void) enterApplication
{
    UIViewController* currentVC = [[self window] rootViewController];
    [currentVC dismissViewControllerAnimated:YES completion:nil];
    
    LandingPageViewController *pvc = [LandingPageViewController new];
    [pvc setSDKReady: self.sdkReady];
    [[self window] setRootViewController:pvc];
    [self.window makeKeyAndVisible];
    
}

-(void) showLoginSignup
{
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"LoginSignupStoryboard" bundle:nil] instantiateInitialViewController];

    [[self window] setRootViewController:vc];
    [self.window makeKeyAndVisible];
    
}


-(BOOL)attemptToSkipLogin
{
    NSString* userID = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USER_ID_KEY];
    BOOL isInvited = [[NSUserDefaults standardUserDefaults] boolForKey:SAVED_USER_INVITE_CODE_KEY];
    NSString *fn = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USER_FIRSTNAME_KEY];
    NSString *sn = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USER_LASTNAME_KEY];
    
    if(fn == nil) {
        fn = @"";
    }
    if(sn == nil) {
        sn = @"";
    }
    
    if(userID != nil || isInvited == YES) {

        [[KrumbsSDK sharedInstance] registerUserWithEmail:userID firstName:fn lastName:sn];
        
        [self enterApplication];
        return YES;
    }
    else {
        // no saved data or previously used as anonymous user
        return NO;
    }
}


-(void) logoutAction:(NSNotification *)notification
{
    // remove saved credentials from NSUserDefaults
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:SAVED_USER_ID_KEY];
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:SAVED_USER_INVITE_CODE_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:SAVED_USER_FIRSTNAME_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:SAVED_USER_LASTNAME_KEY];
    
    UIViewController* currentVC = [[self window] rootViewController];
    [currentVC dismissViewControllerAnimated:YES completion:nil];
    
    [self showLoginSignup];
}

-(void) loginCompletedAction:(NSNotification *)notification
{
    // If this is anonymous login
    NSDictionary *userInfo = [notification userInfo];
    if(userInfo != nil) {
        if([userInfo objectForKey:@"AnonymousLogin"] != nil) {
            // anonymous usage
            KrumbsUserInfo *kuser = [[KrumbsUserInfo alloc]initAsAnonymousUser];
            [[KrumbsSDK sharedInstance] registerUser:kuser];
            [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:SAVED_USER_IS_ANONYMOUS_KEY];

        } else {
            [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:SAVED_USER_IS_ANONYMOUS_KEY];

            
            NSString *email = (NSString *) [[notification userInfo] objectForKey:@"Email"];
            NSString *inviteCode = (NSString *) [[notification userInfo] objectForKey:@"InviteCode"];
            if(email != nil) {
                [[NSUserDefaults standardUserDefaults] setObject:email forKey:SAVED_USER_ID_KEY];
                NSString *fn = (NSString *) [[notification userInfo] objectForKey:@"FirstName"];
                NSString *sn = (NSString *) [[notification userInfo] objectForKey:@"LastName"];
                if(fn != nil) {
                    [[NSUserDefaults standardUserDefaults] setObject:fn forKey:SAVED_USER_FIRSTNAME_KEY];
                }
                if(sn != nil) {
                    [[NSUserDefaults standardUserDefaults] setObject:sn forKey:SAVED_USER_LASTNAME_KEY];
                }
                
                [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:SAVED_USER_INVITE_CODE_KEY];
                
                [[KrumbsSDK sharedInstance] registerUserWithEmail:email firstName:fn lastName:sn];
                
            }
            else if(inviteCode != nil) {
                [[NSUserDefaults standardUserDefaults] setObject:inviteCode forKey:SAVED_USER_ID_KEY];
                [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:SAVED_USER_INVITE_CODE_KEY];
                KrumbsUserInfo *kuser = [[KrumbsUserInfo alloc]initWithUserName:inviteCode firstName:@"" lastName:@""];
                [[KrumbsSDK sharedInstance] registerUser:kuser];
                
            }
            
            
        }
        
    }
    
    [self enterApplication];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [KrumbsSDK shutdown];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
